--	Gets the udder status of sheep on a specific date
--	Udders are No Udder, Small Udder, Medium Udder or Large Udder
--	Assumes the evaluation uses the first custom evaluation location for the udder status.
SELECT 
	sheep_table.sheep_id
	, sheep_table.sheep_name
	, evaluation_trait_table.trait_name
	, custom_evaluation_traits_table.custom_evaluation_item
	, sheep_evaluation_table.trait_score16
	, sheep_evaluation_table.eval_date 
	, sheep_table.alert01

FROM sheep_table
INNER JOIN evaluation_trait_table ON sheep_evaluation_table.trait_name16 = evaluation_trait_table.id_traitid
INNER JOIN custom_evaluation_traits_table ON custom_evaluation_traits_table.id_custom_traitid = sheep_evaluation_table.trait_score16
INNER JOIN  sheep_evaluation_table ON  sheep_evaluation_table.sheep_id = sheep_table.sheep_id 
--	Adjust the date to be the date of the evaluation as required
WHERE sheep_evaluation_table.eval_date like "2021-06-06%"
ORDER BY
	trait_score16