--	Gets all the lambings that were from any form of AI.
SELECT 
--	Uncomment this to get the ewes name in the beginning
--	sheep_table.sheep_name
 	,
	* 
FROM lambing_history_table 
--	Uncomment this if you want to restrict by service types
-- 	JOIN ram_breeding_table ON lambing_history_table.id_breedingid = ram_breeding_table.id_rambreedingid
--	Uncomment this if you want to see the ewe name as well
-- 	INNER JOIN sheep_table on lambing_history_table.dam_id = sheep_table.sheep_id
WHERE 
	gestation_length >120 
	AND gestation_length < 165 
--	Uncomment this to restrict to a specific year 
-- 	AND lambing_date > "2018%"
--	Uncomment this to get all AI breedings. Natural service is type 1. or set to = 1 for natural service only
 	AND ram_breeding_table.id_servicetypeid > 1
ORDER BY 
	lambing_date
