SELECT 
	ewe_breeding_table.ewe_id
	, ewe_table.sheep_name AS ewe_name
	, ewe_breeding_table.id_ewebreedingid
	, ram_breeding_table.id_rambreedingid
	, ram_table.sheep_name AS ram_name
	, ram_breeding_table.date_ram_in
	, ram_breeding_table.time_ram_in
	, ram_breeding_table.date_ram_out
	, ram_breeding_table.time_ram_out
	, ram_breeding_table.id_servicetypeid 
	, service_type_table.service_type
FROM ram_breeding_table
	LEFT OUTER JOIN ewe_breeding_table ON ewe_breeding_table.id_rambreedingid = ram_breeding_table.id_rambreedingid 
	INNER JOIN sheep_table as ram_table ON ram_breeding_table.ram_id = ram_table.sheep_id
	INNER JOIN sheep_table as ewe_table ON ewe_breeding_table.ewe_id = ewe_table.sheep_id
	INNER JOIN service_type_table ON service_type_table.id_servicetypeid = ram_breeding_table.id_servicetypeid 
WHERE 
	(ram_breeding_table.id_servicetypeid = 3
	OR ram_breeding_table.id_servicetypeid = 5)
--	AND date_ram_in LIKE "2019%" 
ORDER BY 
	ewe_name
	, ram_name

--  Another version with sire and dam ages and shows breeding type

SELECT 
	ewe_breeding_table.ewe_id
	, ewe_table.sheep_name AS ewe_name
	, ewe_table.birth_date AS ewe_birth
	, date_ram_in - ewe_table.birth_date + 1 AS ewe_age_at_lambing
	, ewe_breeding_table.id_ewebreedingid
	, ram_breeding_table.ram_id
	, ram_table.sheep_name AS ram_name
	, ram_table.birth_date AS ram_birth
	, date_ram_in - ram_table.birth_date AS ram_age
	, ram_breeding_table.date_ram_in
	, ram_breeding_table.time_ram_in
	, ram_breeding_table.date_ram_out
	, ram_breeding_table.time_ram_out
	, ram_breeding_table.id_servicetypeid 
	, service_type_table.service_type
FROM ram_breeding_table 
	LEFT OUTER JOIN ewe_breeding_table ON ewe_breeding_table.id_ewebreedingid = ram_breeding_table.id_rambreedingid 
	INNER JOIN sheep_table as ram_table ON ram_breeding_table.ram_id = ram_table.sheep_id
	INNER JOIN sheep_table as ewe_table ON ewe_breeding_table.ewe_id = ewe_table.sheep_id
	INNER JOIN service_type_table ON service_type_table.id_servicetypeid = ram_breeding_table.id_servicetypeid
WHERE 
	(ram_breeding_table.id_servicetypeid = 3
	OR ram_breeding_table.id_servicetypeid = 5)
	AND date_ram_in LIKE "2016%" 
ORDER BY 
	ewe_name
	, ram_name

