-- Gets whole list of current sheep properly including sheep without a sire or dam or without EBVs

SELECT 
	sheep_table.sheep_id
	, (SELECT 
		tag_number 
		FROM id_info_table 
		WHERE 
			official_id = "1" 
			AND id_info_table.sheep_id = sheep_table.sheep_id 
			AND  (tag_date_off IS NULL OR tag_date_off = '')) 
		AS fedtag
	, (SELECT 
		tag_number 
		FROM id_info_table 
		WHERE 
			tag_type = "4" 
			AND id_info_table.sheep_id = sheep_table.sheep_id 
			AND  (tag_date_off IS NULL OR tag_date_off = '')) 
		AS farmtag
	,(SELECT 
		tag_number 
		FROM id_info_table 
		WHERE 
			tag_type = "2" 
			AND id_info_table.sheep_id = sheep_table.sheep_id 
			AND (tag_date_off IS NULL OR tag_date_off = '') 
			AND ( id_info_table.official_id IS NULL OR id_info_table.official_id = 0 )) 
		AS eidtag
-- Can add or delete items here by adding or removing -- in front of the item.
	, sheep_table.sheep_name
	, codon171_table.codon171_alleles
	, sheep_ebv_table.usa_maternal_index
	, sheep_ebv_table.self_replacing_carcass_index
	, sheep_ebv_table.ebv_birth_weight
	, sheep_ebv_table.ebv_wean_weight
--	, sheep_ebv_table.ebv_post_wean_weight
--	, sheep_ebv_table.ebv_hogget_weight
--	, sheep_ebv_table.ebv_adult_weight
--	, sheep_ebv_table.ebv_post_wean_scrotal
	, sheep_ebv_table.ebv_number_lambs_born
	, sheep_ebv_table.ebv_number_lambs_weaned
--	, sheep_ebv_table.ebv_maternal_birth_weight
--	, sheep_ebv_table.ebv_maternal_wean_weight
--	, sheep_ebv_table.ebv_lambease_direct
--	, sheep_ebv_table.ebv_lambease_daughter

	, cluster_table.cluster_name
    , sheep_table.birth_date
	, sheep_sex_table.sex_abbrev
	, birth_type_table.birth_type
	, sire_table.sheep_name as sire_name
	, dam_table.sheep_name as dam_name
	, sheep_table.alert01

FROM sheep_table 

INNER JOIN codon171_table ON sheep_table.codon171 = codon171_table.id_codon171id 
LEFT JOIN birth_type_table ON sheep_table.birth_type = birth_type_table.id_birthtypeid 
LEFT JOIN sheep_sex_table ON sheep_table.sex = sheep_sex_table.sex_sheepid 
LEFT JOIN sheep_table AS sire_table ON sheep_table.sire_id = sire_table.sheep_id
LEFT JOIN sheep_table AS dam_table ON sheep_table.dam_id = dam_table.sheep_id
LEFT OUTER JOIN  sheep_cluster_table ON sheep_table.sheep_id = sheep_cluster_table.sheep_id
LEFT  JOIN cluster_table ON cluster_table.id_clusternameid = sheep_cluster_table.which_cluster

--	Edit the date to be the most recent run of EBV data by changing the date below
LEFT OUTER JOIN sheep_ebv_table ON sheep_table.sheep_id = sheep_ebv_table.sheep_id 
--	Modify this to be the latest EBV run date. Should be a way to get this automatically but I haven't figured it out.
	AND sheep_ebv_table.ebv_date LIKE "2018-09%"

WHERE 
	(sheep_table.remove_date 		IS 	NULL 
	OR sheep_table.remove_date 	IS 	'') 

-- To get only a single sex add this in the WHERE clause
-- Edit for sex by making Ram sex = 1 Ewe Sex = 2 Wether sex = 3 
--	AND sheep_table.sex = 1 

-- To get no butcher or sell add this in the WHERE clause
--	AND (sheep_table.alert01 NOT LIKE "%Sell%" 
--		AND sheep_table.alert01 NOT LIKE "%Butcher%")

-- To get only butcher or sell add this in the WHERE clause
--	AND (sheep_table.alert01 LIKE "%Sell%" 
--		OR sheep_table.alert01 LIKE "%Butcher%")

-- To get only a specific alert add this in the WHERE clause
-- Change the text between the % to be what alert you are looking for. 
-- Common ones include Sell, Butcher, Keep and so on.  
--	AND sheep_table.alert01 LIKE "%Sell%"

-- To get only older sheep not current year lambs add this in the WHERE clause changing the year as required
-- To get only this years lambs change the < to a > 
-- 	AND sheep_table.birth_date < "2018%”

ORDER BY 
	sheep_sex_table.sex_abbrev ASC
	, cluster_table.cluster_name
	, sheep_ebv_table.self_replacing_carcass_index DESC
	, sheep_table.birth_date ASC

