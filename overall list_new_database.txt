-- Gets whole list of current sheep properly including sheep without a sire or dam or without EBVs
-- This version only gets sheep located at Contact number 1 which is at Garvin Mesa

SELECT 
	sheep_table.sheep_id
	, (SELECT 
		tag_number 
		FROM id_info_table 
		WHERE 
			official_id = "1" 
			AND id_info_table.sheep_id = sheep_table.sheep_id 
			AND  (tag_date_off IS NULL OR tag_date_off = '')) 
		AS fedtag
	, (SELECT 
		tag_number 
		FROM id_info_table 
		WHERE 
			tag_type = "4" 
			AND id_info_table.sheep_id = sheep_table.sheep_id 
			AND  (tag_date_off IS NULL OR tag_date_off = '')) 
		AS farmtag
--	, (SELECT 
--		tag_color_name
--		FROM tag_colors_table
--          INNER JOIN id_info_table ON tag_colors_table.id_tagcolorsid = id_info_table.tag_color_male
--		WHERE 
--			tag_type = "4" 
--			AND id_info_table.sheep_id = sheep_table.sheep_id 
--			AND  (tag_date_off IS NULL OR tag_date_off = '')) 
--		AS farmtagcolor
--	,(SELECT 
--		tag_number 
--		FROM id_info_table 
--		WHERE 
--			tag_type = "2" 
--			AND id_info_table.sheep_id = sheep_table.sheep_id 
--			AND (tag_date_off IS NULL OR tag_date_off = '') 
--			AND ( id_info_table.official_id IS NULL OR id_info_table.official_id = 0 OR id_info_table.official_id = "")) 
--		AS eidtag
-- Can add or delete items here by adding or removing -- in front of the item.
--	, flock_prefix_table.flock_name
	, sheep_table.sheep_name
--	, codon136_table.codon136_alleles
--	, codon141_table.codon141_alleles
--	, codon154_table.codon154_alleles
	, codon171_table.codon171_alleles
-- 	, sheep_evaluation_table.trait_score11 as weight
-- 	, sheep_evaluation_table.age_in_days
	, sheep_ebv_table.usa_maternal_index
	, sheep_ebv_table.self_replacing_carcass_index
--	, sheep_ebv_table.ebv_birth_weight
--	, sheep_ebv_table.ebv_wean_weight
--	, sheep_ebv_table.ebv_post_wean_weight
--	, sheep_ebv_table.ebv_hogget_weight
--	, sheep_ebv_table.ebv_adult_weight
--	, sheep_ebv_table.ebv_post_wean_scrotal
--	, sheep_ebv_table.ebv_number_lambs_born
--	, sheep_ebv_table.ebv_number_lambs_weaned
--	, sheep_ebv_table.ebv_maternal_birth_weight
--	, sheep_ebv_table.ebv_maternal_wean_weight
--	, sheep_ebv_table.ebv_lambease_direct
--	, sheep_ebv_table.ebv_lambease_daughter
--	, (	
--		sheep_table.birth_type
--		+ sheep_table.codon171 
--		+ sheep_evaluation_table.trait_score01 
--		+ sheep_evaluation_table.trait_score02 
--		+ sheep_evaluation_table.trait_score03 
--		+ sheep_evaluation_table.trait_score04 
--		+ sheep_evaluation_table.trait_score05 
--		+ sheep_evaluation_table.trait_score06 
--		+ sheep_evaluation_table.trait_score07 
--		+ sheep_evaluation_table.trait_score08 
--		+ sheep_evaluation_table.trait_score09 
--		+ sheep_evaluation_table.trait_score10
--		) AS overall_score
--	, sheep_evaluation_table.sheep_rank
	, cluster_table.cluster_name
    , sheep_table.birth_date
	, sex_table.sex_abbrev
	, birth_type_table.birth_type
	, sire_table.sheep_name as sire_name
	, dam_table.sheep_name as dam_name
	, sheep_table.alert01
	
	FROM 
		(SELECT
 			sheep_id, MAX(movement_date)
			, to_id_contactsid
			, id_sheeplocationhistoryid
		FROM sheep_location_history_table
		GROUP BY
 			sheep_id) 
		AS last_movement_date

INNER JOIN sheep_ownership_history_table ON sheep_table.sheep_id = sheep_ownership_history_table.sheep_id 
INNER JOIN sheep_table ON sheep_table.sheep_id = last_movement_date.sheep_id
INNER JOIN codon136_table ON sheep_table.codon136 = codon136_table.id_codon136id 
INNER JOIN codon141_table ON sheep_table.codon141 = codon141_table.id_codon141id 
INNER JOIN codon154_table ON sheep_table.codon154 = codon154_table.id_codon154id 
INNER JOIN codon171_table ON sheep_table.codon171 = codon171_table.id_codon171id 
LEFT JOIN birth_type_table ON sheep_table.birth_type = birth_type_table.id_birthtypeid 
LEFT JOIN sex_table ON sheep_table.sex = sex_table.sex_sheepid 
LEFT JOIN sheep_table AS sire_table ON sheep_table.sire_id = sire_table.sheep_id
LEFT JOIN sheep_table AS dam_table ON sheep_table.dam_id = dam_table.sheep_id
LEFT OUTER JOIN  sheep_cluster_table ON sheep_table.sheep_id = sheep_cluster_table.sheep_id
LEFT  JOIN cluster_table ON cluster_table.id_clusternameid = sheep_cluster_table.id_clusterid
-- LEFT JOIN flock_prefix_table ON sheep_table.id_flockprefixid =flock_prefix_table.flock_prefixid 
LEFT OUTER JOIN sheep_ebv_table ON sheep_table.sheep_id = sheep_ebv_table.sheep_id 
-- INNER JOIN sheep_evaluation_table ON sheep_evaluation_table.sheep_id = sheep_table.sheep_id

--	Edit the date to be the most recent run of EBV data by changing the date below
--	Modify this to be the latest EBV run date. Should be a way to get this automatically .
		AND sheep_ebv_table.ebv_date LIKE "2021-10%"

WHERE 
	sheep_table.death_date = "" 
--	Set to be us as the owner and location but can be edited to handle other places and owners.
	AND  sheep_ownership_history_table.to_id_contactsid = '1'  
--	1 is Black Welsh Mountain
	AND sheep_table.id_sheepbreedid = 1 
	AND  last_movement_date.to_id_contactsid = 1

--	If want to add evaluations add this and uncomment the last INNER JOIN above
--		AND eval_date LIKE "2021-11-%"

-- To get only a single sex add this in the WHERE clause
-- Edit for sex by making Ram sex = 1 Ewe Sex = 2 Wether sex = 3 
--		AND sheep_table.sex = 1 

-- To get no butcher or sell add this in the WHERE clause
--		AND (sheep_table.alert01 NOT LIKE "%Sell%" 
--		AND sheep_table.alert01 NOT LIKE "%Butcher%")

-- To get only butcher or sell add this in the WHERE clause
--		AND (sheep_table.alert01 LIKE "%Sell%" 
--			OR sheep_table.alert01 LIKE "%Butcher%")

-- To get only a specific alert add this in the WHERE clause
-- Change the text between the % to be what alert you are looking for. 
-- Common ones include Sell, Butcher, Keep and so on.  
--		AND sheep_table.alert01 LIKE "%Sell%"
--		AND sheep_table.alert01 NOT LIKE "%Ship%"

-- To get only older sheep not current year lambs add this in the WHERE clause changing the year as required
-- To get only this years lambs change the < to a > 
-- To get a specific year change the < to LIKE 
-- 		AND sheep_table.birth_date < "2021%"

ORDER BY 
	sex_table.sex_abbrev ASC
	, cluster_table.cluster_name
	, sheep_ebv_table.self_replacing_carcass_index DESC
	, sheep_table.birth_date ASC
