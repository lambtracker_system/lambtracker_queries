-- Get Lambs for Registration import 
-- Designed for the tag number format that the American Black Welsh Mountain Sheep Association uses
-- the || is used to add the color and which ear data into the number
SELECT
	sheep_table.sheep_id
	, (SELECT 
		tag_number || '/' ||
		id_location_table. id_location_abbrev  ||'/'||
		tag_colors_table.tag_color_abbrev
		FROM id_info_table 
 		INNER JOIN id_location_table on id_location_table. id_locationid = id_info_table.tag_location
		INNER JOIN  tag_colors_table on tag_colors_table. id_tagcolorsid = id_info_table.tag_color_male
		WHERE 
			official_id = "1" 
			AND id_info_table.sheep_id = sheep_table.sheep_id 
			AND  (tag_date_off IS NULL OR tag_date_off = '')) 
		AS fedtag
	, (SELECT 
		id_location_table. id_location_abbrev ||'/'||
		tag_colors_table.tag_color_abbrev || '/' ||
		tag_number
		FROM id_info_table 
 		INNER JOIN id_location_table on id_location_table. id_locationid = id_info_table.tag_location
		INNER JOIN  tag_colors_table on tag_colors_table. id_tagcolorsid = id_info_table.tag_color_male
		WHERE 
			tag_type = "4" 
			AND id_info_table.sheep_id = sheep_table.sheep_id 
			AND  (tag_date_off IS NULL OR tag_date_off = '')) 
		AS farmtag
	,(SELECT 
		id_location_table. id_location_abbrev ||'/'||
		tag_colors_table.tag_color_abbrev || '/' ||
		tag_number
		FROM id_info_table 
 		INNER JOIN id_location_table on id_location_table. id_locationid = id_info_table.tag_location
		INNER JOIN  tag_colors_table on tag_colors_table. id_tagcolorsid = id_info_table.tag_color_male
		WHERE 
			tag_type = "2" 
			AND id_info_table.sheep_id = sheep_table.sheep_id 
			AND (tag_date_off IS NULL OR tag_date_off = '') 
			AND ( id_info_table.official_id IS NULL OR id_info_table.official_id = 0 )) 
		AS eidtag
	, sheep_table.sheep_name
--	Uncomment if you want to see the individual alerts.
--	, sheep_table.alert01
	, codon171_table.codon171_alleles
	, sheep_table.birth_date
	, sheep_table.death_date
--	numeric version 1 = male 2 = female
--	, sheep_table.sex
--	Abbreviation appripriate to the species so R for Ram E for Ewe
--	, sex_table.sex_abbrev
--	Male or Female as M or F
  	, sex_table.sex_abbrev_standard
	, birth_type_table.id_birthtypeid
	, sheep_table.birth_weight
--	Uncomment the sire and dam ID fields if needed for debugging. 
--	They are the internal database ID numbers.
--	, sire_table.sheep_id AS sire_id
	, sire_registration_table.registration_number AS sire_registration
	, sire_table.sheep_name AS sire_name
--	, dam_table.sheep_id AS dam_id
	, dam_registration_table.registration_number AS dam_registration
	, dam_table.sheep_name AS dam_name 
FROM sheep_table 
INNER JOIN codon171_table on sheep_table.codon171 = codon171_table.id_codon171id 
LEFT JOIN birth_type_table on sheep_table.birth_type = birth_type_table.id_birthtypeid 
LEFT JOIN sex_table on sheep_table.sex = sex_table.sex_sheepid 
LEFT JOIN sheep_table as sire_table on sheep_table.sire_id = sire_table.sheep_id
LEFT JOIN sheep_table as dam_table on sheep_table.dam_id = dam_table.sheep_id
LEFT OUTER JOIN  sheep_cluster_table on sheep_table.sheep_id = sheep_cluster_table.sheep_id
LEFT JOIN cluster_table on cluster_table.id_clusternameid = sheep_cluster_table.id_clusterid
LEFT OUTER JOIN sheep_ebv_table on sheep_table.sheep_id = sheep_ebv_table.sheep_id and sheep_ebv_table.ebv_date like "2015-06%"
LEFT JOIN 
	sheep_registration_table on  sheep_table.sheep_id = sheep_registration_table.sheep_id AND 
	sheep_registration_table.registration_contact = 25
LEFT JOIN sheep_registration_table as sire_registration_table on  sire_table.sheep_id = sire_registration_table.sheep_id AND 
	sire_registration_table.registration_contact = 25
LEFT JOIN sheep_registration_table as dam_registration_table on  dam_table.sheep_id = dam_registration_table.sheep_id AND 
	dam_registration_table.registration_contact = 25
WHERE 
	sheep_table.birth_date > "2020%"
ORDER BY
	sheep_table.birth_date ASC
