-- Code to create all the current LambTracker database tables

CREATE TABLE "birth_type_table" ("id_birthtypeid" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE , "birth_type" TEXT, "birth_type_abbrev" TEXT)

CREATE TABLE "cluster_calculation_type_table" ("id_cluster_calculation_typeid" INTEGER PRIMARY KEY  NOT NULL ,"cluster_calculation_type" TEXT)

CREATE TABLE [cluster_table] ([id_clusternameid] INTEGER PRIMARY KEY, [cluster_name] TEXT, "cluster_name_display_order" INTEGER)

CREATE TABLE [codon136_table] ([id_codon136id] INTEGER PRIMARY KEY AUTOINCREMENT, [codon136_alleles] TEXT, "codon136_display_order")

CREATE TABLE [codon154_table] ([id_codon154id] INTEGER PRIMARY KEY AUTOINCREMENT, [codon154_alleles] TEXT, "codon154_display_order" INTEGER)

CREATE TABLE [codon171_table] ([id_codon171id] INTEGER PRIMARY KEY AUTOINCREMENT, [codon171_alleles] TEXT, "codon171_display_order" INTEGER)

CREATE TABLE "contacts_country_table" ("id_contacts_countryid" INTEGER PRIMARY KEY  NOT NULL , "country_name" TEXT, "country_abbreviation" TEXT)

CREATE TABLE "contacts_premise_table" ("id_premiseid" INTEGER PRIMARY KEY  NOT NULL  UNIQUE , "premise_number" TEXT, "premise_latitude" REAL, "premise_longitude" REAL, "premise_altitude" REAL, "altitude_units" INTEGER)

CREATE TABLE "contacts_state_table" ("id_contacts_stateid" INTEGER PRIMARY KEY  NOT NULL , "state_name" TEXT, "state_abbrev" TEXT, "id_contacts_countryid" INTEGER)

CREATE TABLE "contacts_table" ("id_contactsid" INTEGER PRIMARY KEY ,"contact_last_name" TEXT DEFAULT (NULL) ,"contact_first_name" Text,"contact_middle_name" Text,"contact_company" Text,"contact_address1" text,"contact_address2" text,"contact_city" text,"contact_postcode" text,"id_contacts_countryid" INTEGER DEFAULT (1) ,"contact_main_phone" text,"contact_cell_phone" text,"contact_fax" TEXT,"contact_state" text,"contact_email" text,"contact_website" TEXT,"id_premiseid" INTEGER DEFAULT (null) ,"contact_title" INTEGER DEFAULT (null) )

CREATE TABLE "contacts_title_table" ("id_contactstitleid" INTEGER PRIMARY KEY  NOT NULL , "contacts_title" TEXT)

CREATE TABLE [custom_evaluation_name_table] ([id_custom_evalid] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE, [id_traitid] INTEGER REFERENCES [evaluation_trait_table] ([id_traitid]), [custom_eval_number] Integer)

CREATE TABLE [custom_evaluation_traits_table] ([id_custom_traitid] INTEGER PRIMARY KEY DEFAULT(NULL), [id_traitid] INTEGER DEFAULT(NULL) REFERENCES [evaluation_trait_table] ([id_traitid]), [custom_evaluation_item] Text DEFAULT(NULL), [custom_evaluation_order] Integer)

CREATE TABLE "death_reason_table" ("id_deathreasonid" INTEGER PRIMARY KEY  DEFAULT (null) ,"death_reason" TEXT DEFAULT (null) ,"death_reason_display_order" INTEGER DEFAULT (null) , "registry_id_contactsid" INTEGER)

CREATE TABLE [drug_location_table] ([id_druglocationid] INTEGER PRIMARY KEY NOT NULL UNIQUE, [drug_location_name] TEXT, [drug_location_abbrev] TEXT, "drug_location_display_order" INTEGER DEFAULT null)

CREATE TABLE [drug_location_table] ([id_druglocationid] INTEGER PRIMARY KEY NOT NULL UNIQUE, [drug_location_name] TEXT, [drug_location_abbrev] TEXT, "drug_location_display_order" INTEGER DEFAULT null)

CREATE TABLE "drug_table" ("id_drugid" INTEGER PRIMARY KEY ,"drug_type" INTEGER,"official_drug_name" TEXT,"drug_lot" TEXT,"drug_expire_date" TEXT,"drug_purchase_date" TEXT,"drug_meat_withdrawal" INTEGER,"meat_withdrawal_units" INTEGER,"user_meat_withdrawal" INTEGER,"generic_drug_name" TEXT,"drug_dispose_date" TEXT,"official_drug_dosage" TEXT,"drug_amount_purchased" TEXT,"drug_cost" REAL,"off_label" BOOLEAN,"off_label_vet" INTEGER,"user_drug_dosage" TEXT,"user_task_name" TEXT,"drug_gone" BOOLEAN DEFAULT (null) , "drug_cost_units" INTEGER, "drug_removable" BOOLEAN, "drug_milk_withdrawal" INTEGER, "milk_withdrawal_units" INTEGER, "user_milk_withdrawal" INTEGER)

CREATE TABLE [drug_type_table] ([id_drugtypeid] INTEGER PRIMARY KEY AUTOINCREMENT, [drug_type] TEXT, "drug_type_display_order" INTEGER)

CREATE TABLE "ebv_calculation_method_table" ("id_ebvmethodid" INTEGER PRIMARY KEY  NOT NULL  UNIQUE , "calculation_method" TEXT, "ebv_calculation_method_display_order" INTEGER)

CREATE TABLE "ebv_cross_reference_table" ("id_ebvcrossrefid" INTEGER PRIMARY KEY  NOT NULL ,"lambtracker_table_name" TEXT NOT NULL  DEFAULT (null) ,"lambtracker_field_name" TEXT NOT NULL  DEFAULT (null) , "lambplan_field_name" TEXT)

CREATE TABLE "ebv_date_ranges" ("id_ebvdateid" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE , "age_name" TEXT, "lambplan_minimum_days" INTEGER, "lambplan_maximum_days" INTEGER, "nsip_minimum_days" INTEGER, "nsip_maximum_days" INTEGER, "user_minimum_days" INTEGER, "user_maximum_days" INTEGER, "lambplan_optimal_age" INTEGER, "nsip_optimal_age" INTEGER, "user_optimal_age" INTEGER)

CREATE TABLE "embryo_freezing_method_table" ("id_embryofreezingmethodid" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE , "embryo_freezing_method_name" TEXT UNIQUE , "embryo_freezing_method_display_order" INTEGER)

CREATE TABLE "embryo_grade_table" ("id_embryogradeid" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE , "embryo_grade_name" TEXT UNIQUE , "embryo_grade_display_order" INTEGER)

CREATE TABLE "embryo_stage_table" ("id_embryostageid" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE , "embryo_stage_name" TEXT UNIQUE , "embryo_stage_display_order" INTEGER)

CREATE TABLE "evaluation_trait_table" ("id_traitid" INTEGER PRIMARY KEY ,"trait_name" CHAR NOT NULL ,"trait_type" INTEGER,"evaluation_trait_display_order" INTEGER DEFAULT (null) )

CREATE TABLE "ewe_breeding_table" ("id_ewebreedingid" INTEGER PRIMARY KEY  DEFAULT (null) ,"ewe_id" INTEGER DEFAULT (null) ,"id_rambreedingid" INTEGER DEFAULT (null) )

CREATE TABLE "external_file_type_table" ("id_externalfiletypeid" INTEGER PRIMARY KEY  NOT NULL  DEFAULT (null) ,"external_file_type" TEXT DEFAULT (null) ,"external_file_type_suffix" TEXT DEFAULT (null) , "external_file_type_display_order" INTEGER)

CREATE TABLE "farm_location_table" ("id_farmlocationid" INTEGER PRIMARY KEY  DEFAULT (null) ,"farm_location_name" TEXT DEFAULT (null) ,"farm_location_display_order" INTEGER,"farm_location_abbreviation" TEXT)

CREATE TABLE "feed_external_file_table" ("id_feedexternalfileid" INTEGER PRIMARY KEY  NOT NULL  DEFAULT (null) ,"id_feedingredientid" INTEGER DEFAULT (null) ,"feed_external_file_path" TEXT DEFAULT (null) ,"feed_external_file_date" TEXT DEFAULT (null) ,"feed_external_file_time" TEXT DEFAULT (null) ,"feed_external_file_type" INTEGER DEFAULT (null) )

CREATE TABLE "feed_ingredient_table"(
	"id_feedingredientid"              Integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	"feed-Ingredient_name"             Text NOT NULL,
	"feed_ingredient_contactsID"       Integer,
	"feed_ingredient_cost"             Real,
	"feed_ingredient_cost_units"       Integer,
	"feed_ingredient_amount_purchased" Real DEFAULT null,
	"feed_ingredient_amount_units"     Integer,
	"feed_purchase_date"               Text,
	"feed_remove_date"                 Text DEFAULT null,
	"feed_gone"                        Boolean,
UNIQUE ( "id_feedingredientid" ) )

CREATE TABLE "fleece_weight_adjustment_table" ("id_fleeceadjustmentid" INTEGER PRIMARY KEY  NOT NULL ,"adjustment_name" TEXT,"ewe_age_2yr" REAL,"ewe_age_3yr" REAL,"ewe_age_4_5yr" REAL,"ewe_age_6_7yr" REAL DEFAULT (null) ,"ewe_age_8yr_up" REAL, "source_reference" TEXT)

CREATE TABLE [flock_prefix_table] ([flock_prefixid] INTEGER PRIMARY KEY AUTOINCREMENT, [flock_name] TEXT NOT NULL DEFAULT 'Desert Weyr', "registry_id_contactsid" INTEGER)

CREATE TABLE "id_info_table" ("id_infoid" Integer PRIMARY KEY ,"sheep_id" Integer,"tag_type" Integer,"tag_color_male" Integer,"tag_color_female" Integer,"tag_location" Integer,"tag_date_on" Text DEFAULT (null) ,"tag_date_off" Text DEFAULT (null) ,"tag_number" Text,"id_flockid" Integer,"official_id" BOOL DEFAULT (null) ,"id_removetagreasonid" INTEGER DEFAULT (null) )

CREATE TABLE [id_location_table] ([id_locationid] INTEGER PRIMARY KEY AUTOINCREMENT, [id_location_name] TEXT NOT NULL DEFAULT 'Right Ear', [id_location_abbrev] TEXT (2) NOT NULL DEFAULT 'RE')

CREATE TABLE "id_remove_reason_table" ("id_idremovetagreasonid" INTEGER PRIMARY KEY  NOT NULL  DEFAULT (null) ,"id_remove_reason" TEXT)

CREATE TABLE [id_type_table] ([id_typeid] INTEGER PRIMARY KEY AUTOINCREMENT, [idtype_name] TEXT NOT NULL DEFAULT 'Federal', "id_type_abbrev" TEXT, "id_type_display_order" INTEGER)

CREATE TABLE "lambing_history_table" ("id_lambinghistoryid" INTEGER PRIMARY KEY  DEFAULT (null) ,"lambing_date" DATE,"dam_id" INTEGER,"sire_id" INTEGER,"lambing_notes" TEXT,"lambs_born" INTEGER (1),"lambs_weaned" INTEGER (1),"lambing_time" TEXT,"gestation_length" INTEGER,"lambing_contact" INTEGER,"id_breedingid" INTEGER)

CREATE TABLE "lambs_born_adjustment_table" ("id_lambsbornadjustmentid" INTEGER PRIMARY KEY  NOT NULL , "adjustment_name" TEXT, "ewe_age_1yr" REAL, "ewe_age_2yr" REAL, "ewe_age_3yr" REAL, "ewe_age_4yr" REAL, "ewe_age_5yr" REAL, "ewe_age_6yr" REAL, "ewe_age_7yr" REAL, "ewe_age_8yr" REAL, "ewe_age_9yr_up" REAL, "source_reference" TEXT)

CREATE TABLE "lambs_born_table" ("id_lambsbornid" INTEGER PRIMARY KEY  NOT NULL ,"id_lambinghistoryid" INTEGER NOT NULL  DEFAULT (null) ,"sheep_id" INTEGER NOT NULL ,"birth_order" INTEGER NOT NULL , "foster_eweid" INTEGER, "hand_reared" BOOL)

CREATE TABLE "lambtracker_default_settings" ("id_defaultsid" INTEGER PRIMARY KEY  NOT NULL ,"id_contactsid" INTEGER,"eid_tag_color_male" INTEGER,"eid_tag_color_female" INTEGER,"farm_tag_based_on_eid_tag" BOOL,"farm_tag_number_digits_from_eid" INTEGER,"farm_tag_color_male" INTEGER,"farm_tag_color_female" INTEGER,"fed_tag_color_male" INTEGER,"fed_tag_color_female" INTEGER,"eid_tag_location" INTEGER DEFAULT (null) ,"farm_tag_location" INTEGER DEFAULT (null) ,"fed_tag_location" INTEGER,"evaluation_update_alert" BOOL DEFAULT (null) ,"sheep_breed" INTEGER,"flock_prefix" INTEGER DEFAULT (null) ,"birth_weight_units" INTEGER,"early_gestation_length" INTEGER,"late_gestation_length" INTEGER, "sale_price_units" INTEGER)

CREATE TABLE [last_eval_table] ([id_lastevalid] INTEGER PRIMARY KEY AUTOINCREMENT DEFAULT(NULL), [id_traitid] INTEGER REFERENCES [evaluation_trait_table] ([id_traitid]), [id_unitsid] INTEGER REFERENCES [units_table] ([id_unitsid]))

CREATE TABLE "management_group_table" ([id_managementgroupid] INTEGER PRIMARY KEY UNIQUE, [management_group] TEXT, "management_group_display_order" INTEGER)

CREATE TABLE "membership_region_table" ("id_membershipregionid" INTEGER PRIMARY KEY  NOT NULL ,"membership_region" TEXT,"membership_region_number" INTEGER,"membership_region_display_order" INTEGER,"registry_id_contactsid" INTEGER DEFAULT (null) )

CREATE TABLE "owner_registration_table" ("id_ownerregistrationid" INTEGER PRIMARY KEY  NOT NULL ,"owner_contactid" INTEGER NOT NULL ,"membership_number" TEXT NOT NULL  DEFAULT (null) ,"registry_contactid" INTEGER NOT NULL ,"date_joined" TEXT,"dues_paid_until" TEXT,"date_resigned" TEXT, "online_password" TEXT, "membership_type" INTEGER, "region" INTEGER, "flock_prefixid" INTEGER)

CREATE TABLE "predefined_notes_table" ("id_predefinednotesid" INTEGER PRIMARY KEY ,"predefined_note_text" TEXT,"predefined_note_display_order" INTEGER DEFAULT (null) )

CREATE TABLE "ram_breeding_table" ("id_rambreedingid" INTEGER PRIMARY KEY  DEFAULT (null) ,"ram_id" INTEGER,"date_ram_in" TEXT,"time_ram_in" TEXT,"date_ram_out" TEXT,"time_ram_out" TEXT,"id_servicetypeid" INTEGER DEFAULT (null) ,"id_storedsemenid" INTEGER DEFAULT (null) )

CREATE TABLE "registry_membership_type_table" ("id_registrymembershiptypeid" INTEGER PRIMARY KEY  NOT NULL ,"registry_id_contactsid" INTEGER DEFAULT (null) ,"membership_type" TEXT,"membership_abbrev" TEXT,"membership_type_display_order" INTEGER)

CREATE TABLE "scrapie_flock_table" ("id_scrapieflockid" INTEGER PRIMARY KEY  DEFAULT (null) ,"scrapie_flockid" TEXT,"scrapie_flock_display_order" INTEGER, "id_contactsid" INTEGER)

CREATE TABLE "semen_extender_table" ("id_semenextenderid" INTEGER PRIMARY KEY  DEFAULT (null) ,"semen_extender_name" TEXT DEFAULT (null) , "semen_extender_display_order" INTEGER)

CREATE TABLE "service_type_table" ("id_servicetypeid" INTEGER PRIMARY KEY ,"service_type" TEXT,"service_abbrev" TEXT,"service_type_display_order" INTEGER DEFAULT (null) )

CREATE TABLE "sex_table" ("sex_sheepid" INTEGER PRIMARY KEY ,"sex_name" TEXT NOT NULL  DEFAULT (null) ,"sex_abbrev" TEXT,"sex_standard" TEXT, "sex_abbrev_standard" TEXT, "sex_display_order" INTEGER)

CREATE TABLE "sheep_breed_table" ("id_sheepbreedid" INTEGER PRIMARY KEY ,"sheep_breed_name" TEXT,"sheep_breed_abbrev" TEXT,"sheep_breed_display_order" INTEGER,"registry_id_contactsid" INTEGER DEFAULT (null) )

CREATE TABLE "sheep_cluster_table" ("id_sheepclusterid" INTEGER PRIMARY KEY  DEFAULT (null) ,"sheep_id" INTEGER,"cluster_date" TEXT,"id_clusterid" INTEGER DEFAULT (null) ,"id_clustercalculationtypeid" Integer DEFAULT (null) ,"cluster_calculation_id_contactid" INTEGER)

CREATE TABLE [sheep_drug_table] ([id_sheepdrugid] INTEGER PRIMARY KEY AUTOINCREMENT, [sheep_id] INTEGER REFERENCES [sheep_table] ([sheep_id]), [drug_id] INTEGER REFERENCES [drug_table] ([id_drugid]), [drug_date_on] TEXT, [drug_time_on] TEXT, [drug_date_off] TEXT, [drug_time_off] TEXT, [drug_dosage] TEXT, [drug_location] INTEGER REFERENCES [drug_location_table] ([id_druglocationid]))

CREATE TABLE "sheep_ebv_table" ("id_sheepebvid" INTEGER PRIMARY KEY  NOT NULL ,"sheep_id" INTEGER,"ebv_date" TEXT,"usa_maternal_index" REAL,"maternal_dollar_index" REAL,"maternal_dollar_acc" REAL,"ebv_birth_weight" REAL,"ebv_birth_weight_acc" REAL,"ebv_wean_weight" REAL,"ebv_wean_weight_acc" REAL,"ebv_maternal_birth_weight" REAL,"ebv_maternal_birth_weight_acc" REAL,"ebv_maternal_wean_weight" REAL,"ebv_maternal_wean_weight_acc" REAL,"ebv_post_wean_weight" REAL,"ebv_post_wean_weight_acc" REAL,"ebv_yearling_weight" REAL,"ebv_yearling_weight_acc" REAL,"ebv_hogget_weight" REAL,"ebv_hogget_weight_acc" REAL,"ebv_adult_weight" REAL,"ebv_adult_weight_acc" REAL,"ebv_wean_fat" REAL,"ebv_wean_fat_acc" REAL,"ebv_post_wean_fat" REAL,"ebv_post_wean_fat_acc" REAL,"ebv_yearling_fat" REAL,"ebv_yearling_fat_acc" REAL,"ebv_hogget_fat" REAL,"ebv_hogget_fat_acc" REAL,"ebv_wean_emd" REAL,"ebv_wean_emd_acc" REAL,"ebv_post_wean_emd" REAL,"ebv_post_wean_emd_acc" REAL,"ebv_yearling_emd" REAL,"ebv_yearling_emd_acc" REAL,"ebv_hogget_emd" REAL,"ebv_hogget_emd_acc" REAL,"ebv_wean_fec" REAL,"ebv_wean_fec_acc" REAL,"ebv_post_wean_fec" REAL,"ebv_post_wean_fec_acc" REAL,"ebv_yearling_fec" REAL,"ebv_yearling_fec_acc" REAL,"ebv_hogget_fec" REAL,"ebv_hogget_fec_acc" REAL,"ebv_post_wean_scrotal" REAL,"ebv_post_wean_scrotal_acc" REAL,"ebv_yearling_scrotal" REAL,"ebv_yearling_scrotal_acc" REAL,"ebv_hogget_scrotal" REAL,"ebv_hogget_scrotal_acc" REAL,"ebv_number_lambs_born" REAL,"ebv_number_lambs_born_acc" REAL,"ebv_number_lambs_weaned" REAL,"ebv_number_lambs_weaned_acc" REAL,"ebv_yearling_fleece_diameter" REAL,"ebv_yearling_fleece_diameter_acc" REAL,"ebv_hogget_fleece_diameter" REAL,"ebv_hogget_fleece_diameter_acc" REAL,"ebv_adult_fleece_diameter" REAL,"ebv_adult_fleece_diameter_acc" REAL,"ebv_yearling_greasy_fleece_weight" REAL,"ebv_yearling_greasy_fleece_weight_acc" REAL,"ebv_hogget_greasy_fleece_weight" REAL DEFAULT (null) ,"ebv_hogget_greasy_fleece_weight_acc" REAL,"ebv_adult_greasy_fleece_weight" REAL,"ebv_adult_greasy_fleece_weight_acc" REAL,"ebv_yearling_clean_fleece_weight" REAL,"ebv_yearling_clean_fleece_weight_acc" REAL DEFAULT (null) ,"ebv_hogget_clean_fleece_weight" REAL,"ebv_hogget_clean_fleece_weight_acc" REAL,"ebv_adult_clean_fleece_weight" REAL,"ebv_adult_clean_fleece_weight_acc" REAL,"ebv_yearling_fleece_yield" REAL,"ebv_yearling_fleece_yield_acc" REAL,"ebv_hogget_fleece_yield" REAL,"ebv_hogget_fleece_yield_acc" REAL,"ebv_adult_fleece_yield" REAL,"ebv_adult_fleece_yield_acc" REAL,"ebv_yearling_fiber_diameter_variation" REAL,"ebv_yearling_fiber_diameter_variation_acc" REAL,"ebv_hogget_fiber_diameter_variation" REAL DEFAULT (null) ,"ebv_hogget_fiber_diameter_variation_acc" REAL,"ebv_adult_fiber_diameter_variation" REAL,"ebv_adult_fiber_diameter_variation_acc" REAL,"ebv_yearling_staple_strength" REAL,"ebv_yearling_staple_strength_acc" REAL,"ebv_hogget_staple_strength" REAL,"ebv_hogget_staple_strength_acc" REAL,"ebv_adult_staple_strength" REAL,"ebv_adult_staple_strength_acc" REAL,"ebv_yearling_staple_length" REAL,"ebv_yearling_staple_length_acc" REAL,"ebv_hogget_staple_length" REAL,"ebv_hogget_staple_length_acc" REAL,"ebv_adult_staple_length" REAL,"ebv_adult_staple_length_acc" REAL,"ebv_yearling_fleece_curvature" REAL,"ebv_yearling_fleece_curvature_acc" REAL,"ebv_hogget_fleece_curvature" REAL,"ebv_hogget_fleece_curvature_acc" REAL,"ebv_adult_fleece_curvature" REAL,"ebv_adult_fleece_curvature_acc" REAL,"ebv_lambease_direct" REAL,"ebv_lambease_direct_acc" REAL,"ebv_lambease_daughter" REAL,"ebv_lambease_daughter_acc" REAL,"ebv_gestation_length" REAL,"ebv_gestation_length_acc" REAL,"ebv_gestation_length_daughter" REAL,"ebv_gestation_length_daughter_acc" REAL,"self_replacing_carcass_index" REAL,"self_replacing_carcass_acc" REAL,"self_replacing_carcass_no_repro_index" REAL,"self_replacing_carcass_no_repro_acc" REAL,"carcass_plus_index" REAL,"carcass_plus_acc" REAL,"lamb_2020_index" REAL,"lamb_2020_acc" REAL DEFAULT (null) ,"maternal_dollar_no_repro_index" REAL,"maternal_dollar_no_repro_acc" REAL,"dual_purpose_dollar_index" REAL,"dual_purpose_dollar_acc" REAL,"dual_purpose_dollar_no_repro_index" REAL,"dual_purpose_dollar_no_repro_acc" REAL, "which_run" TEXT, "ebv_yearling_number_lambs_born" REAL, "ebv_yearling_number_lambs_born_acc" REAL, "ebv_yearling_number_lambs_weaned" REAL, "ebv_yearling_number_lambs_weaned_acc" REAL, "border_dollar_index" REAL, "border_dollar_acc" REAL, "spare_index" REAL, "spare_index_acc" REAL, "coopworth_dollar_index" REAL, "coopworth_dollar_acc" REAL, "spare_2_index" REAL, "spare_2_index_acc" REAL, "export_index" REAL, "trade_index" REAL, "merino_dp_index" REAL, "merino_dp_acc" REAL, "merino_dp_plus_index" REAL, "merino_dp_plus_acc" REAL, "fine_medium_index" REAL, "fine_medium_acc" REAL, "fine_medium_plus_index" REAL, "fine_medium_plus_acc" REAL, "samm_index" REAL, "samm_acc" REAL, "dohne_no_repro_index" REAL, "dohne_no_repro_acc" REAL, "superfine_index" REAL, "superfine_acc" REAL, "superfine_plus_index" REAL, "superfine_plus_acc" REAL, "maternal_greasy_fleece_weight" REAL, "maternal_greasy_fleece_weight_acc" REAL, "maternal_clean_fleece_weight" REAL, "maternal_clean_fleece_weight_acc" REAL, "early_breech_wrinkle" REAL, "early_breech_wrinkle_acc" REAL, "late_breech_wrinkle" REAL, "late_breech_wrinkle_acc" REAL, "early_body_wrinkle" REAL, "early_body_wrinkle_acc" REAL, "late_body_wrinkle" REAL, "late_body_wrinkle_acc" REAL, "late_dag" REAL, "late_dag_acc" REAL, "ebv_calculation_method" INTEGER, "intramuscular_fat" REAL, "intramuscular_fat_acc" REAL, "carcass_shear_force" REAL, "carcass_shear_force_acc" REAL, "dressing_percentage" REAL, "dressing_percentage_acc" REAL, "lean_meat_yield" REAL, "lean_meat_yield_acc" REAL, "hot_carcass_weight" REAL, "hot_carcass_weight_acc" REAL, "carcass_fat" REAL, "carcass_fat_acc" REAL, "carcass_eye_muscle_depth" REAL, "carcass_eye_muscle_depth_acc" REAL)

CREATE TABLE "sheep_evaluation_table" ("id_evaluationid" INTEGER PRIMARY KEY ,"sheep_id" INTEGER NOT NULL ,"trait_name01" INTEGER,"trait_score01" REAL,"trait_name02" Integer,"trait_score02" real,"trait_name03" Integer,"trait_score03" real,"trait_name04" integer,"trait_score04" real,"trait_name05" integer,"trait_score05" real,"trait_name06" integer,"trait_score06" real,"trait_name07" integer,"trait_score07" real,"trait_name08" integer,"trait_score08" real,"trait_name09" integer,"trait_score09" real,"trait_name10" integer,"trait_score10" real,"trait_name11" integer,"trait_score11" real,"trait_name12" integer,"trait_score12" real,"trait_name13" integer,"trait_score13" real,"trait_name14" integer,"trait_score14" real,"trait_name15" integer,"trait_score15" real,"eval_date" TEXT DEFAULT (NULL) ,"trait_units11" INTEGER,"trait_units12" INTEGER,"trait_units13" INTEGER,"trait_units14" INTEGER,"trait_units15" INTEGER,"sheep_rank" INTEGER,"number_sheep_ranked" INTEGER,"trait_name16" INTEGER,"trait_score16" INTEGER,"trait_name17" INTEGER,"trait_score17" INTEGER,"trait_name18" INTEGER,"trait_score18" INTEGER,"trait_name19" INTEGER,"trait_score19" INTEGER,"trait_name20" INTEGER,"trait_score20" INTEGER,"eval_time" TEXT,"age_in_days" INTEGER DEFAULT (null) )

CREATE TABLE "sheep_external_file_table" ("id_sheepexternalfileid" INTEGER PRIMARY KEY  NOT NULL  DEFAULT (null) ,"sheep_id" INTEGER,"sheep_external_file_path" TEXT DEFAULT (null) ,"sheep_external_file_date" TEXT DEFAULT (null) ,"sheep_external_file_time" TEXT DEFAULT (null) ,"sheep_external_file_type" INTEGER DEFAULT (null) )

CREATE TABLE "sheep_farm_location_history_table" ("id_sheepfarmlocationhistoryid" INTEGER PRIMARY KEY  NOT NULL  DEFAULT (null) ,"sheep_id" INTEGER,"id_farmlocationid" INTEGER DEFAULT (null) ,"farm_location_date_in" TEXT DEFAULT (null) ,"farm_location_date_out" TEXT DEFAULT (null) )

CREATE TABLE "sheep_feed_table" ("id_sheepfeedid" INTEGER PRIMARY KEY  NOT NULL ,"sheep_id" INTEGER NOT NULL ,"feed_ingredient_id" INTEGER NOT NULL ,"start_date_feed" TEXT NOT NULL ,"end_date_feed" TEXT,"feed_serving_size" REAL,"feed_serving_size_units" INTEGER)

CREATE TABLE "sheep_location_history_table" ("id_sheeplocationhistoryid" INTEGER PRIMARY KEY  NOT NULL  DEFAULT (null) ,"sheep_id" INTEGER,"movement_date" TEXT DEFAULT (null) ,"from_id_contactsid" INTEGER,"to_id_contactsid" INTEGER DEFAULT (null) )

CREATE TABLE "sheep_note_table" ("id_noteid" INTEGER PRIMARY KEY ,"sheep_id" INTEGER,"note_text" TEXT,"note_date" TEXT,"note_time" TEXT,"id_predefinednotesid01" INTEGER)

CREATE TABLE "sheep_ownership_history_table" ("id_sheepownershipid" INTEGER PRIMARY KEY  NOT NULL  DEFAULT (null) ,"sheep_id" INTEGER,"transfer_date" TEXT DEFAULT (null) ,"from_id_contactsid" INTEGER DEFAULT (NULL) ,"to_id_contactsid" INTEGER,"id_transferreasonid" INTEGER DEFAULT (null) ,"sell_price" REAL DEFAULT (null) ,"sell_price_units" INTEGER)

CREATE TABLE "sheep_registration_table" ("id_sheepregistrationid" INTEGER PRIMARY KEY  DEFAULT (null) ,"sheep_id" INTEGER,"registration_number" TEXT,"registration_contact" INTEGER,"id_sheep_registrationtypeid" INTEGER DEFAULT (null) )

CREATE TABLE "sheep_registration_type_table" ("id_sheep_registrationtypeid" INTEGER PRIMARY KEY  NOT NULL , "sheep_registration_type" TEXT, "registration_type_abbrev" TEXT, "registration_type_display_order" INTEGER, "id_contactsid" INTEGER)

CREATE TABLE "sheep_table" ("sheep_id" INTEGER PRIMARY KEY ,"sheep_name" TEXT,"id_flockprefixid" INTEGER DEFAULT (null) ,"sex" INTEGER (1),"birth_date" TEXT,"birth_type" INTEGER (1),"birth_weight" REAL,"rear_type" INTEGER (1),"death_date" TEXT,"lambease" INTEGER (1),"sire_id" INTEGER,"dam_id" INTEGER,"alert01" TEXT,"sheep_birth_record" INTEGER,"codon171" INTEGER,"codon154" INTEGER,"codon136" INTEGER,"nsip_id" TEXT,"id_sheepbreedid" INTEGER,"birth_weight_units" INTEGER,"birth_time" TEXT,"id_breederid" INTEGER DEFAULT (NULL) ,"weaned_date" TEXT,"inbreeding" REAL,"management_group" INTEGER, "death_reason" INTEGER)

CREATE TABLE "stored_embryo_table"(
	"id_storedembryoid"         Integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	"sire_id"                   Integer,
	"dam_id"                    Integer,
	"number_embryos_total"      Integer DEFAULT null,
	"goblet_identifier"         Text,
	"date_collected"            Text,
	"time_collected"            Text,
	"date_ewe_estrus"           Text,
	"id_embryofreezingmethodid" Integer,
	"id_embryogradeid"          Integer,
	"id_embryostageid"          Integer,
	"location_collectedid"      Integer,
	"vet_collectedid"           Integer,
	"number_embryos_per_straw"  Integer,
	"id_ewebreedingid"          Integer )

CREATE TABLE "stored_semen_table" ("id_storedsemenid" INTEGER PRIMARY KEY  DEFAULT (null) ,"sheep_id" INTEGER DEFAULT (null) ,"number_straws" INTEGER DEFAULT (null) ,"straws_per_Insemination" TEXT DEFAULT (null) ,"extender_id" INTEGER DEFAULT (null) ,"post_thaw_motility" REAL DEFAULT (null) ,"post_thaw_progressive_motility" REAL DEFAULT (null) ,"motile_sperm_per_insemination" INTEGER DEFAULT (null) ,"goblet_identifier" TEXT DEFAULT (null) ,"date_collected" TEXT,"time_collected" TEXT,"owner_id_contactsid" INTEGER DEFAULT (null) , "location_id_contactsid" INTEGER)

CREATE TABLE "tag_colors_table" ("id_tagcolorsid" INTEGER PRIMARY KEY  DEFAULT (null) ,"tag_color_abbrev" TEXT NOT NULL  DEFAULT ('Y') ,"tag_color_name" TEXT NOT NULL  DEFAULT ('Yellow') ,"tag_color_display_order" INTEGER DEFAULT (null) )

CREATE TABLE "trait_type_table" ("id_traittypeid" INTEGER PRIMARY KEY  DEFAULT (null) ,"trait_type" CHAR,"trait_type_display_order" INTEGER)

CREATE TABLE "transfer_reason_table" ("id_transferreasonid" INTEGER PRIMARY KEY  DEFAULT (null) ,"transfer_reason" TEXT DEFAULT (null) ,"transfer_reason_display_order" INTEGER DEFAULT (null) )

CREATE TABLE "weaning_adjustment_table" ("id_weaningadjustmentid" INTEGER PRIMARY KEY, "adjustment_name" TEXT, "ewe_age_1yr" REAL, "ewe_age_2yr" REAL, "ewe_age_3_6yr" REAL, "ewe_age_over_6yr" REAL, "lamb_sex_ram" REAL, "lamb_sex_ewe" REAL, "lamb_sex_wether" REAL, "birth_and_rear_11" REAL, "birth_and_rear_12" REAL, "birth_and_rear_21" REAL, "birth_and_rear_22" REAL, "birth_and_rear_31" REAL, "birth_and_rear_32" REAL, "birth_and_rear_33" REAL, "source_reference" TEXT)


